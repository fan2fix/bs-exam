layui.use(['layer','carousel','util','element'], function(){
  var carousel = layui.carousel;
  var element = layui.element;
  var layer = layui.layer;
  //建造实例
  carousel.render({
    elem: '#test1'
   // ,width:'100%' //设置容器宽度
    //,height:'80%' 
    ,full:true
    ,arrow: 'always' //始终显示箭头
   
    //,anim: 'updown' //切换动画方式
  });
  var util = layui.util;
  //执行
  util.fixbar({
    bar1: true
    ,bar2: true
    ,css: {right: 0, bottom: 25}
    ,click: function(type){
      console.log(type);
      if(type === 'bar1'){
    	  layer.open({
    		  type: 2,
    		  shade: false,
    		  title:"时间点",
    		 // area: '60%',
    		  height:'70%', //设置容器宽度
    		  width:'70%', 
    		  maxmin: true,
    		  content: '/timeline',
    		  anim: 1,
    		  zIndex: layer.zIndex, //重点1
    		  success: function(layero){
    		    layer.setTop(layero); //重点2
    		  }
    		}); 
      }
      if(type === 'bar2'){
    	  layer.open({
    		  type: 2,
    		  shade: false,
    		  title:"时间点",
    		 // area: '60%',
    		  height:'70%', //设置容器宽度
    		  width:'70%', 
    		  maxmin: true,
    		  content: '/timeline',
    		  anim: 1,
    		  zIndex: layer.zIndex, //重点1
    		  success: function(layero){
    		    layer.setTop(layero); //重点2
    		  }
    		}); 
      }
    }
  });
});



//var projectPath = '${pageContext.request.contextPath}';

//var item1 = {
//	img : 'static/img/cute.png', //图片 
//	info : '在你的存款还没500万之前，你所有的理想跟爱好都应该是赚钱!', //文字 
//	href : '', //链接 
//	close : true, //显示关闭按钮 
//	speed : 8, //延迟,单位秒,默认6 
//	color : '#ffffff', //颜色,默认白色 
//	old_ie_color : '#ffffff', //ie低版兼容色,不能与网页背景相同,默认黑色 
//}
//$('body').barrager(item1);



var isUpdateData = false; //是否需要刷新数据

$(function(){
	showBarrage();
})

//弹幕
function testBarrager(){
	if($("#text").val().length>15){
		layer.msg("弹幕最多输入20字");
	}
	if($("#text").val().length==0){
		layer.msg("请输入弹幕");
	}
	var item2 = {
			img : '/img/love.jpg', //图片 
			info :$("#text").val(), //文字 
			href : '', //链接 
			close : true, //显示关闭按钮 
			speed : 18, //延迟,单位秒,默认6 
			color : 'white', //颜色,默认白色 
			old_ie_color : '#000000', //ie低版兼容色,不能与网页背景相同,默认黑色 
		}
	if($("#text").val().length()>15){
		layer.msg("弹幕最多输入15字");
	}
	$.ajax({
		    //几个参数需要注意一下
	        type: "POST",//方法类型
	        dataType: "json",//预期服务器返回的数据类型
	        data: item2,
	        url: "/barrager", //url
	        success: function (result) {
	           //打印服务端返回的数据(调试用)
	            if (result.code == '0000') {
	            	console.log(result.msg);
	            	console.log(result.msg);
	            	$("#text").val("");
	            }else{
	            	console.log(result.msg);
	            	console.log(result.msg);
	            };
	        },
	        error : function(result) {
	        	console.log(result.msg);
	        }
	    }); 
	 isUpdateData =true;
	$('body').barrager(item2);
}

//清除所有的弹幕
function cleanBarrager(){
	$.fn.barrager.removeAll();
}

function testJson(){
	var testText = document.getElementById("text").value;
	var jsonData = '{"message":testText, "age":"12"}';
	var json = eval('(' + jsonData + ')');
	console.log(json.message);
}


//从服务器端获取弹幕信息并显示所有的弹幕信息
function showBarrage() {
	$.ajaxSettings.async = false;
	$.getJSON('/barragerData', function(data) {
		//每条弹幕发送间隔
		var items = data;
		//弹幕总数
		var total = data.length;
		
		var looper_time = 5 * 1000;
		//是否首次执行
		var run_once = true;
		
		//弹幕索引
		var index = 0;
		//先执行一次
		if(total <=0){return;}
		barrager();
		function barrager() {
			if (run_once) {
				//如果是首次执行,则设置一个定时器,并且把首次执行置为false
				looper = setInterval(barrager, looper_time);
				run_once = false;
			}
			//发布一个弹幕
			$('body').barrager(items[index]);
			//索引自增
			index++;
			//所有弹幕发布完毕，清除计时器。
			if (index == total) {
				index=0;
				if(isUpdateData){
					$.getJSON('/barragerData', function(newdata) {
						items =newdata;
						total = data.length;
					});
				}
//				clearInterval(looper);
//				return false;
			}
		}
	});
}


