//Demo
var form;
$(function(){
	layui.use(['form','rate'], function(){
		form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
		
		    var rate = layui.rate;	   
		    //渲染
		    var ins1 = rate.render({
		      elem: '#rate'  //绑定元素
	    	  ,setText: function(value){
	    		    var arrs = {
	    		      '1': '极差'
	    		      ,'2': '差'
	    		      ,'3': '中等'
	    		      ,'4': '好'
	    		    };
	    		    this.span.text(arrs[value] || ( value + "星"));
	    		  }
		  });

		init(); 
		form.on('submit(formDemo)', function(data){
			var param ={};
			var examId=$("#subject").val();
			var answers =[];
			var undoneNum =0;
			$("input[name='answer']").each(function(){
				answers.push($(this).val());
				if(isBlank($(this).val())){
					undoneNum++;
				}
			})
			param.answer =answers; 
			param.examId =examId;
			layer.msg(JSON.stringify(param));

			if(undoneNum > 0){
				layer.confirm('还有'+undoneNum+'题未做，确定要提交吗', {
					btn: ['继续答题', '提交'] //可以无限个按钮
				}, function(index, layero){
					layer.close(index);
					return false;
				}, function(index){
					submit(param);
				});
			}else{
				submit(param);
			}
			return false;
		});


		/*		form.on('radio(optionA)', function(data){
			console.log(data.elem); //得到radio原始DOM对象
			console.log(data.value); //被点击的radio的value值
			$("#quesText").append("<input type='text' name='answer' lay-verify='answer' class='layui-input'>")
			   layui.use('element', function() {
		            var element = layui.element;
		            element.init();
		        });
		}); */
	})
})


function init(){
	$("#subject").val(1);
	$.ajax({
		url : "/exam?id=1",
		type : 'GET',
		cache:false,
		async:false,
		success : function(data) {
			$("#subject").append("<legend><b>"+data.data[0].subject+"</b></legend>");
			layui.use('element', function() {
				var element = layui.element;
				element.init();
			});
			$.ajax({
				url : "/question?examId=1&qusType=1",
				type : 'GET',
				cache:false,
				async:false,
				success:function(data) {
					var questions =data.data; 
					for (var i = 0; i <questions.length; i++) {
						$("#question").append("<div class='layui-col-xs12 layui-col-sm12 layui-col-md12'>" +
								"<h4 style='margin: 20px;'><b>"
								+"&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' style ='width: 20px' name ='answer'></input>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
								+questions[i].qusIndex+"."+questions[i].question+"</b></h4></div>");
						$("#question").append(
								/*"<div class='layui-form-item' >"+*/
								"<div class='layui-input-block'  style='margin-top: 15px;'>"+
								/*"<div class='layui-col-xs12 layui-col-sm12 layui-col-md12'> " +*/ //移动：6/12 | 平板：6/12 | 桌面：4/12
								"<p class='option' style='cursor:pointer;margin-top:15px;width=300px;' onclick='choice(this)'> A."+questions[i].optionA+"</p>"+
								"<p class='option' style='cursor:pointer;margin-top: 15px;' onclick='choice(this)'> B."+questions[i].optionB+"</p>"+
								"<p class='option' style='cursor:pointer;margin-top: 15px;' onclick='choice(this)'> C."+questions[i].optionC+"</p>"+
								"<p class='option' style='cursor:pointer;margin-top: 15px;' onclick='choice(this)'> D."+questions[i].optionD+"</p>"+
								/*"</div>" +*/
								/*"</div>" +*/
						"</div>");
					}
					layui.use('element', function() {
						var element = layui.element;
						element.init();
					});
				}
			})
		}

	})
	form.render();
}

function choice(option){
	var optionText=$(option).text();
	var answer =optionText.split('.')[0];
	console.log(answer); //被点击的radio的value值
	console.log($(option).parent().text());
	/*console.log($(option).parent().prev().children("input").html());*/
	console.log($(option).parent().prev().children().children().first());
	$(option).parent().prev().children().children().children().first().val(answer);
	layui.use('element', function() {
		var element = layui.element;
		element.init();
	});
}


function isBlank(str) {
	return (!str || /^\s*$/.test(str));
}

function submit(param) {
	$.ajax({
		url : "/submit",
		type :'POST',
		data:JSON.stringify(param) ,
		contentType:"application/json; charset=utf-8",
		dataType: "json",
		cache:false,
		async:false,
		success : function(data) {
			$("#score").html(data.data);
			$("input").attr("disabled","disabled");
			$("button").attr("disabled","disabled");
			$('.option').removeAttr('onclick');
			if(data.data > 6){
				layer.open({
					title: '考试结果'
						,content: '恭喜考试通过 得分：'+data.data
						,time:'3000'
							,anim: 1
				});  
			}else{
				layer.open({
					title: '考试结果'
						,content: '考试未通过 得分：'+data.data
						,time:'3000'
							,anim: 1
				});  
			}
			$('body,html').animate({scrollTop:0},300);
		}
	});

}

/*layui.use('form', function(){
  var form = layui.form;

  //监听提交
  form.on('submit(formDemo)', function(data){
    layer.msg(JSON.stringify(data.field));
    return false;
  });
});*/




