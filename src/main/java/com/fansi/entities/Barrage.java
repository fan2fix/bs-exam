package com.fansi.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 弹幕实体类
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Barrage {
	private String name;
	private String img;
	private String info;
	private String href;
	private boolean close;
	private int speed;
	private String color;
	private String old_ie_color;
}