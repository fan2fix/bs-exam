package com.fansi.service;

import java.util.List;

import com.fansi.entities.Question;

public interface QuestionSevice {
	
	public List<Question> getQuestion(Question question);

}
