package com.fansi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fansi.controller.req.ReqSubmitExam;
import com.fansi.entities.Exam;
import com.fansi.entities.ExamExample;
import com.fansi.entities.Question;
import com.fansi.mapper.ExamMapper;
import com.fansi.service.ExamSevice;
import com.fansi.service.QuestionSevice;

@Service
public class ExamServiceImpl implements ExamSevice {
	@Autowired
	ExamMapper examMapper;
	@Autowired
	QuestionSevice questionSevice;
	@Override
	public List<Exam> getExamList(Exam exam) {
		ExamExample examExample=new ExamExample();
		ExamExample.Criteria criteria =examExample.createCriteria();
		
		if(!StringUtils.isEmpty(exam.getId())) {
			criteria.andIdEqualTo(exam.getId());
		}
		if(!StringUtils.isEmpty(exam.getRound())) {
			criteria.andRoundEqualTo(exam.getRound());
		}
		if(!StringUtils.isEmpty(exam.getUserNo())) {
			criteria.andUserNoEqualTo(exam.getUserNo());
		}
		examExample.setOrderByClause("round asc");
		List<Exam>	exams=examMapper.selectByExample(examExample);
		return exams;
	}
	
	@Override
	public Double ratingScore(ReqSubmitExam reqGetExam) {
		List<String> answers =reqGetExam.getAnswer();
		Question getQuestion = new Question();
		getQuestion.setExamId(reqGetExam.getExamId());
		getQuestion.setQusType(1);
		List<Question> questions =questionSevice.getQuestion(getQuestion);
		double score =0d;
		for (int i =0;i< questions.size() ;i++) {
			if(questions.get(i).getAnswer().trim().equals(answers.get(i).trim())) {
				score=score+questions.get(i).getScore();
			}
		}
		return score;
	}

}
