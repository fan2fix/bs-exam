package com.fansi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fansi.entities.Question;
import com.fansi.entities.QuestionExample;
import com.fansi.mapper.QuestionMapper;
import com.fansi.service.QuestionSevice;

@Service
public class QuestionSeviceImpl implements QuestionSevice {
	@Autowired
	QuestionMapper questionMapper;
	
	@Override
	public List<Question> getQuestion(Question question) {
		QuestionExample questionExample=new QuestionExample();
		QuestionExample.Criteria criteria =questionExample.createCriteria();
		if(!StringUtils.isEmpty(question.getQusType())) {
			criteria.andQusTypeEqualTo(question.getQusType());
		}
		if(!StringUtils.isEmpty(question.getQusType())) {
			criteria.andExamIdEqualTo(question.getExamId());
		}
		questionExample.setOrderByClause("qus_type,qus_index asc");
		List<Question>	questions=questionMapper.selectByExample(questionExample);
		return questions;
	}
}
