package com.fansi.service.impl;

import com.fansi.controller.req.ReqUserLoginVo;
import com.fansi.controller.req.ReqUserRegisterVo;
import com.fansi.controller.resp.ResultBean;
import com.fansi.rpc.UserService;

public class UserFallback implements UserService {

	@Override
	public ResultBean existUser(String name) {
		return  new ResultBean(ResultBean.getFail(),"搜索用户姓名失败，请重试");
	}

	@Override
	public ResultBean login(ReqUserLoginVo requser) {
		return new ResultBean(ResultBean.getFail(),"登入失败，请重试");
	}

	@Override
	public ResultBean register(ReqUserRegisterVo requser) {
		return new ResultBean(ResultBean.getFail(),"注册失败，请重试");
	}

	@Override
	public ResultBean getUser(String name) {
		return new ResultBean(ResultBean.getFail(),"注册失败，请重试");
	}

}
