package com.fansi.service;

import java.util.List;

import com.fansi.controller.req.ReqSubmitExam;
import com.fansi.entities.Exam;

public interface ExamSevice {
	
	public List<Exam> getExamList(Exam exam);

	public Double ratingScore(ReqSubmitExam reqGetExam);

}
