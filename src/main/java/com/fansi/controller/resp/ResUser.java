package com.fansi.controller.resp;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class ResUser {
	private Long id;

    private String name;
    
    private String userName;

    private String password;

    private String email;

    private String wechatNo;

    private String creator;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    private String modifier;

    private Date modifiedTime;
    
    private String role;
    
    private String headPortrait;

    
    
}
