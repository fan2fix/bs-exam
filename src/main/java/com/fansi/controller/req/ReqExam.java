package com.fansi.controller.req;

import lombok.Data;

@Data
public class ReqExam extends ReqPageVo {

    private Long id;

   
    private String no;

   
    private Integer round;

  
    private String subject;

   
    private String userNo;

  
    private Integer score;
    
}
