package com.fansi.controller.req;

import lombok.Data;

@Data
public class ReqPageVo {
	
	protected int pageNum;
	protected int pageSize;

}
