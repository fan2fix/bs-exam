package com.fansi.controller.req;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 弹幕实体类
 *
 */
@Data
public class ReqBarrage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -10173336868789922L;
	private String name;
	private String img;
	private String info;
	private String href;
	private boolean close;
	private int speed;
	private String color;
	private String old_ie_color;

	public static void main(String[] args) {
		ReqBarrage barrage =new ReqBarrage();
		barrage.setName("帆帆");
		barrage.setInfo("");
	}
}