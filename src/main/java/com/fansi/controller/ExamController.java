package com.fansi.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fansi.controller.req.ReqExam;
import com.fansi.controller.req.ReqSubmitExam;
import com.fansi.controller.resp.ResultBean;
import com.fansi.entities.Exam;
import com.fansi.service.ExamSevice;

@Controller
public class ExamController {
	@Autowired
	ExamSevice examSevice;

	@RequestMapping("/list")
	public String list(){
		return "list";
	}
	
	@RequestMapping("/main")
	public String main(){
		return "main";
	}
	
	
	@RequestMapping("/examPage")
	public String exam(){
		return "exam/list";
	}
	
  @RequestMapping(value = "about",method = RequestMethod.GET)
    public String about(){
        return "about";
    }
	
	@RequestMapping("/exam")
	@ResponseBody
	public ResultBean<List<Exam>> getExamList(ReqExam reqGetExam){
		Exam exam=new Exam();
		BeanUtils.copyProperties(reqGetExam, exam);
		List<Exam> exams=examSevice.getExamList(exam);
		return new ResultBean<List<Exam>> (exams);
	}
	
	
	@PostMapping(value = "/submit",produces="application/json; charset=utf-8")
	@ResponseBody
	public ResultBean<String> submit(@RequestBody ReqSubmitExam reqGetExam){

		Double score=examSevice.ratingScore(reqGetExam);
		
		return new ResultBean<String> (String.valueOf(score));
	}
	
	
	
/*	@RequestMapping("/index")
	public String index(){
		return "login";
	}*/
}
