package com.fansi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fansi.controller.req.ReqBarrage;
import com.fansi.controller.req.ReqUserLoginVo;
import com.fansi.controller.req.ReqUserRegisterVo;
import com.fansi.controller.resp.ResUser;
import com.fansi.controller.resp.ResultBean;
import com.fansi.entities.Barrage;
import com.fansi.rpc.UserService;
import com.fansi.util.RedisUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	@Autowired
	RedisUtil redisUtil;
	@Autowired
	ObjectMapper objectMapper;
	
	public static final String LOGIN_USER ="logining_user_";
	
	
	@RequestMapping("/index")
	public String index(){
		return "login";
	}
	
	
	@RequestMapping("/reg")
	public String register(){
		return "register";
	}
	
	@RequestMapping("/love")
	public ModelAndView love(@RequestParam("name")String name) throws JsonParseException, JsonMappingException, IOException{
		ModelAndView mav =new ModelAndView();
		Object userStrData=redisUtil.get(LOGIN_USER+name);
		if(StringUtils.isEmpty(userStrData)) {
			mav.setViewName("login");
			return mav;
		} 
		ResUser resUser=objectMapper.readValue((String)userStrData, ResUser.class);
		mav.setViewName("love");
		mav.addObject("user", resUser);
		return mav;
	}
	
	@RequestMapping("/timeline")
	public String timeline(){
		return "timeline";
	}
	

	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/existUser",method=RequestMethod.GET)
	@ResponseBody
	public ResultBean existUser(String name) {
		ResultBean result=userService.existUser(name);
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/user",method=RequestMethod.GET)
	@ResponseBody
	public ResultBean getUser(String name) {
		ResultBean result=userService.getUser(name);
		return result;
		
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public ResultBean login(@RequestBody ReqUserLoginVo requser) throws IOException {
//		Enumeration headerNames = request.getHeaderNames();
//		while (headerNames.hasMoreElements()) {
//			String key = (String) headerNames.nextElement();
//			System.out.println(key + ": " + request.getHeader(key));
//		}
		ResultBean result=null;
		Object userStrData =redisUtil.get(LOGIN_USER+requser.getName());
		if(!StringUtils.isEmpty(userStrData)) {
			ResUser resUser=objectMapper.readValue((String)userStrData, ResUser.class);
			result=new ResultBean(userStrData);
			return result;
		}
		result=userService.login(requser);
		if(ResultBean.SUCCESS.equals(result.getCode())) {
			redisUtil.set(LOGIN_USER+requser.getName(),objectMapper.writeValueAsString(result.getData()),30*60);
		}
		//ResUser resUser =(ResUser)(result.getData());
		return result;
		
	}
	
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/register",method=RequestMethod.POST)
	@ResponseBody
	public ResultBean register(@RequestBody ReqUserRegisterVo requser) {
//		Enumeration headerNames = request.getHeaderNames();
//		while (headerNames.hasMoreElements()) {
//			String key = (String) headerNames.nextElement();
//			System.out.println(key + ": " + request.getHeader(key));
//		}
		ResultBean result=userService.register(requser);
		//ResUser resUser =(ResUser)(result.getData());
		return result;
		
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/barragerData",method=RequestMethod.GET)
	@ResponseBody
	public List<Barrage> barragerData() throws JsonParseException, JsonMappingException, IOException {
		List<Barrage> list = new ArrayList<Barrage>();
		ObjectMapper mapper = new ObjectMapper();
		//barrager
		List<Object> range = redisUtil.listGet("barrager", 0, -1);
		 for (Object object : range) {
			 list.add(mapper.readValue((String)object, Barrage.class));
	      }
		return list;
	}
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="/barrager",method=RequestMethod.POST)
	@ResponseBody
	public ResultBean barrager(ReqBarrage barrage) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		//listOperations.rightPush("barrager", mapper.writeValueAsString(barrage));
		redisUtil.listSet("barrager", mapper.writeValueAsString(barrage));
		return new ResultBean();	
	}
	
//	img : '/img/love.jpg', //图片 
//	info :$("#text").val(), //文字 
//	href : '', //链接 
//	close : true, //显示关闭按钮 
//	speed : 18, //延迟,单位秒,默认6 
//	color : '#ffffff', //颜色,默认白色 
//	old_ie_color : '#ffffff', //ie低版兼容色,不能与网页背景相同,默认黑色 
	
	

}
