package com.fansi.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fansi.controller.req.ReqQuestion;
import com.fansi.controller.resp.ResultBean;
import com.fansi.entities.Question;
import com.fansi.service.ExamSevice;
import com.fansi.service.QuestionSevice;

@Controller
public class QuestionController {
	@Autowired
	ExamSevice examSevice;
	@Autowired
	QuestionSevice questionSevice;

	
	@RequestMapping("/question")
	@ResponseBody
	public ResultBean<List<Question>> getQuestion(ReqQuestion reqQuestion){
		Question question=new Question();
		BeanUtils.copyProperties(reqQuestion, question);
		List<Question> questions=questionSevice.getQuestion(question);
		return new ResultBean<List<Question>>(questions);
	}

	
	
/*	@RequestMapping("/index")
	public String index(){
		return "login";
	}*/
}
