package com.fansi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan
public class WebConfig extends WebMvcConfigurerAdapter {
	
    public WebConfig(){
        super();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/*").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX+"/templates/");
    registry.addResourceHandler("/*").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX+"/static/");
    // registry.addResourceHandler("/**").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX+"/static/images/love/");
//     registry.addResourceHandler("/img/*").addResourceLocations("file:/Users/lfz/img/images/love/");
//     registry.addResourceHandler("/audio/*").addResourceLocations("file:/Users/lfz/audio/");
     super.addResourceHandlers(registry);  
      }

    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //拦截规则：除了login，其他都拦截判断
       //registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**").excludePathPatterns("/user/*","/index","/error");
        super.addInterceptors(registry);
    }
    
    @Override
	public void addViewControllers(ViewControllerRegistry registry) {
    	registry.addRedirectViewController("/", "index");
    	registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    	 super.addViewControllers(registry);
	}

}