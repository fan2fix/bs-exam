package com.fansi.rpc;

import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fansi.controller.req.ReqUserLoginVo;
import com.fansi.controller.req.ReqUserRegisterVo;
import com.fansi.controller.resp.ResUser;
import com.fansi.controller.resp.ResultBean;
import com.fansi.service.impl.UserFallback;


@FeignClient(name="user",fallback = UserFallback.class)
public interface UserService {
	
	@RequestMapping(value="/existUser",method=RequestMethod.GET)
	public ResultBean existUser(@RequestParam("name") String name);

	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ResultBean<ResUser> login(@RequestBody ReqUserLoginVo requser);
	
	@RequestMapping(value="/user",method=RequestMethod.POST)
	public ResultBean register(@RequestBody ReqUserRegisterVo requser);
	
	@RequestMapping(value="/getUser",method=RequestMethod.GET)
	public ResultBean getUser(@RequestParam("name") String name);
}


