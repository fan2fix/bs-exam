package com.fansi;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@MapperScan(basePackages="com.fansi.mapper")
public class App{  
//extends ResourceServerConfigurerAdapter {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(App.class, args);
	}
	
	@Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));// 日期格式化输出
        mapper.setTimeZone(TimeZone.getDefault());
//        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
//        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
//        mapper.configure(JsonGenerator.Feature.QUOTE_NON_NUMERIC_NUMBERS, true);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);// 忽略未知属性
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper;
    }
//	@Override
//	public void configure(HttpSecurity http) throws Exception {
//		http
//		.csrf().disable()
//		.authorizeRequests()
//		.antMatchers("/**").authenticated()
//		.antMatchers(HttpMethod.GET, "/index")
//		.hasAuthority("WRIGTH_READ");
//	}
//
//	@Override
//	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//		resources
//		.resourceId("WRIGTH")
//		.tokenStore(jwtTokenStore());
//	}
//
//	@Bean
//	protected JwtAccessTokenConverter jwtTokenConverter() {
//		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//		converter.setSigningKey("springcloud123");
//		return converter;
//	}
//
//	@Bean
//	public TokenStore jwtTokenStore() {
//		return new JwtTokenStore(jwtTokenConverter());
//	}
}
